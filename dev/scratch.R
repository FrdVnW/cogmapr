concept.romania  <-   EdgSocCMap(S4E, units= "country_romania")$from %>%
                                                              c(EdgSocCMap(S4E, units= "country_romania")$to) %>%
                                                              unique()

ConceptIndicators(S4E, units = "country_belgium") %>%
    dplyr::filter(!(num %in% concept.romania))
