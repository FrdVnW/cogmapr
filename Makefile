PKG=.
PARALLEL=TRUE
FILTER=
QUIET=TRUE

LIB=.checks

.checks:
	mkdir -p .checks

rox:
	Rscript --no-save -e 'devtools::document("$(PKG)")'

test:
	Rscript --no-save -e 'devtools::test("$(PKG)", filter="$(FILTER)")'

check: .checks
	Rscript --no-save -e 'devtools::check("$(PKG)", check_dir = "$(LIB)")'

build:
	Rscript --no-save -e 'devtools::build("$(PKG)")'

install: rox
	R CMD INSTALL $(PKG)

manual: rox
	rm -f $(PKG).pdf
	R CMD Rd2pdf -o $(PKG).pdf $(PKG)

roxygen/clean:
	@rm -rf $(PKG)/man $(PKG)/NAMESPACE

clean: roxygen/clean 
	@rm -rf .checks *.Rcheck .Rd2*  $(PKG)/src/*.o $(PKG)/src/*.so $(PKG)/src/*.gcda

