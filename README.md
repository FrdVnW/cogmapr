
# Table of Contents

1.  [Description](#org4a7779d)
2.  [Installation](#org6489b4f)
    1.  [The coding tool : qcoder](#org6e58180)
    2.  [cogmapr](#orgb75a895)
        1.  [Special dependencies](#org5be7a19)
        2.  [The `cogmapr` package](#org5cf2631)
    3.  [cogmapvisualizr](#org8453045)
3.  [Usage](#orgba3b088)
    1.  [`qcoder`](#orgc3be3ea)
        1.  [Load package](#org851216a)
        2.  [Creation and parametrization of a new project](#orgf853ef3)
        3.  [The `qcoder` application](#orgcf05742)
    2.  [`cogmapr`](#org9a4141e)
        1.  [Load packages `cogmapr` and others dependencies](#org50a18d7)
        2.  [Project configuration](#org46b79d8)
        3.  [Some basic functions (see help on functions of `cogmapr` for more possibilities)](#orga1c2224)
    3.  [`cogmapvisualizr`](#orgf4a189f)
4.  [References](#orgc9c0301)



<a id="org4a7779d"></a>

# Description

`'cogmapr'` is an R-package that aims (i) to generated cognitive maps based on the coding of qualitative documents and (ii) to help researchers to analyse these cognitive maps. This approach was firstly developed for analysing systems of practices in social-ecological systems (CMASOP<sup><a id="fnr.1" class="footref" href="#fn.1">1</a></sup>), but can be useful for qualitative analyses.

`'cogmapr'` is the core of a series of three R-packages that works together : 

-   `'qcoder'`, particularilly, the forked version developped for cognitive mapping
-   `'cogmapr'`, all r-base functions for building and analysing cognitive maps (exporting, plots, quotes, documents, &#x2026;)
-   `'cogmapvisualizr'`, a shiny app that makes the usage of `'cogmapr'` easier

This user's manual covers the usage of these three tools. A first section is devoted to the installation of them and the second section to their usage.


<a id="org6489b4f"></a>

# Installation


<a id="org6e58180"></a>

## The coding tool : qcoder

We developped a fork of the project ['ropenscilabs/qcoder'](https://www.github.com/ropenscilabs/qcoder) that aims to code qualitative documents (interviews, &#x2026;) using the CMASOP approach.<sup><a id="fnr.2" class="footref" href="#fn.2">2</a></sup>

The package is hosted on Github and can be installed using the `'devtools'`.

    devtools::install_github('FrdVnW/qcoder', ref = "cogmap-dev", upgrade = 'never')


<a id="orgb75a895"></a>

## cogmapr


<a id="org5be7a19"></a>

### Special dependencies

'cogmapr' depend on `'graph'` and `'Rgraphviz'`, two packages that are not available on CRAN but hosted on 'Bioconductor'.

    install.packages("BiocManager")
    BiocManager::install(c("graph", "Rgraphviz"), update = FALSE)


<a id="org5cf2631"></a>

### The `cogmapr` package

The `cogmapr` package is available on CRAN. 

    install.packages("cogmapr")

The development branches and previous releases are hosted on Gitlab and can be installed in `R` using 'devtools'. These can be installed as follow.

    devtools::install_git("https://gitlab.com/FrdVnW/cogmapr", branch = "dev", upgrade = 'never')
    devtools::install_git("https://gitlab.com/FrdVnW/cogmapr", branch = "RELEASE_0.9.1", upgrade = 'never')


<a id="org8453045"></a>

## cogmapvisualizr

`'cogmapvisualizr'` is an R-package that contains a shiny app for an easy way of using the main functions of `'cogmapr'`. It is hosted on gitlab and can be installed using 'devtools'.

    devtools::install_git("https://gitlab.com/FrdVnW/cogmapvisualizr", ref = "master", upgrade = 'never')


<a id="orgba3b088"></a>

# Usage


<a id="orgc3be3ea"></a>

## `qcoder`


<a id="org851216a"></a>

### Load package

    library(qcoder)


<a id="orgf853ef3"></a>

### Creation and parametrization of a new project

1.  Creation of a new project

        create_qcoder_cogmap_project("PROJECT_NAME", sample = TRUE)
    
    This command will set up a new repository, which is a folder 'PROJECT<sub>NAME</sub>' and a series of important subfolders. 
    If `sample =` TRUE=, basic examples files are copied in the subfolders (4 documents, a small concepts list and some units informations).

2.  Import

    Before the import, optionnally :
    
    -   edit the `'concepts.csv'` file in the folder 'concepts' <sup><a id="fnr.3" class="footref" href="#fn.3">3</a></sup>
    -   add more documents in the folder 'documents',
    -   edit the units (class of documents).
    
    Importing the main parameters will initiate the data frames, in the data frames subfolders.
    
        import_project_data("PROJECT_NAME")
    
    **BE CAREFUL ! Do not import project data if you have already coded your documents or a part of them !**

3.  Some project variables

    Three variables have to be defined before launching the application. These variables can be adapted/changed each time the project is launched (but this is not recomanded).
    
        project_document_part <- c(
            "Subject A",
            "Subject B",
            "Subject C"
        )
        project_coding_class <- c(
            "Relationship Class x",
            "Relationship Class y",
            "Relationship Class z"
        )
        project_concept_class <- c(
            "Concept Class i",
            "Concept Class j",
            "Concept Class k"
        )


<a id="orgcf05742"></a>

### The `qcoder` application

1.  Launch the application

    **If you already started your projects and coded your documents, you can start here.**
    
    This will serve a shiny application reachable locally (127.0.0.1).
    
        qcode()

2.  Coding the documents

    For coding your documents using qcoder, we proposed a small screencast.
    
    <iframe width="560" height="315" src="https://www.youtube.com/embed/ExhNaWau4dE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


<a id="org9a4141e"></a>

## `cogmapr`

The 'cogmapr' package contains functions that can be used for plotting cognitive maps using scripts and terminal. Both *base plot* and *ggplot2* outputs are proposed.


<a id="org50a18d7"></a>

### Load packages `cogmapr` and others dependencies

    library("cogmapr")
    library("dplyr")
    library("pander")
    library("ggplot2")
    library("Rgraphviz")


<a id="org46b79d8"></a>

### Project configuration

`'PROJECT_NAME'` is a project made by the `'qcoder'` package, using the `'cogmap'` way, as described here above. The main path is the path in your computer where this project is stored, (terminating by "/"). By default, it is assumed to be in the working directory of your R session.

    project_name <- "PROJECT_NAME"
    main_path <- paste0(getwd(),'/')
    my.cogmap.project <- ProjectCMap(main_path,project_name)


<a id="orga1c2224"></a>

### Some basic functions (see help on functions of `cogmapr` for more possibilities)

The following commands or group of commands generate, in this order : 

-   a plot, the individual cognitive map (ICM) of the first document,
-   the list of relationships and quotes of the first document,
-   a plot, the social cognitive map (SCM) of all documents, but selecting the relationship with a minimum weight of 2.

    plot(IndCMap(my.cogmap.project, doc.id = '1'))
    
    QuotesIndCMap(my.cogmap.project,1)
    
    plot(
        SocCMap(
    	EdgSocCMap(my.cogmap.project,
    		   min.weight = 2
    		   ),
    	my.cogmap.project,
    	label="name"
        )
    )

These functions may need a LaTeX installation on your computer (to be enhanced, maybe adding other formats).

    SheetCoding(my.cogmap.project)
    ## ReportICM(my.cogmap.project) ## bug
    ## ReportSCM(my.cogmap.project) ## bug


<a id="orgf4a189f"></a>

## `cogmapvisualizr`

The `'cogmapvisualizr'` application can be launch with these commands. (**!! still need `'cogmapr'` and the [3.2.2](#org46b79d8), here above**).

    library(cogmapvisualizr)
    cogmapvisualizr()


<a id="orgc9c0301"></a>

# References

Frédéric M Vanwindekens, Didier Stilmant, and Philippe V Baret. Development of a broadened cognitive mapping approach for analysing systems of practices in social–ecological systems. Ecological modelling, 250:352–362, 2013.

Frédéric M Vanwindekens, Philippe V Baret, and Didier Stilmant. A new approach for comparing and categorizing farmers’ systems of practice based on cognitive mapping and graph theory indicators. Ecological Modelling, 274:1–11, 2014.

Frédéric M Vanwindekens, Didier Stilmant, and Philippe V Baret. Using R in transdisciplinary approaches for visualiazing and analysing people’s perceptions, knowledge and practices in complex social-ecological systems. In The R User Conference, useR! 2017 July 4-7 2017 Brussels, Belgium, page 251, 2017.


# Footnotes

<sup><a id="fn.1" href="#fnr.1">1</a></sup> Vanwindekens et al., 2013

<sup><a id="fn.2" href="#fnr.2">2</a></sup> The CMASOP approach was initially developped using RQDA. This package is no longer maintained and not supported on newer systems (R, linux&#x2026;). It depends on linux packages `GTK+3.0`, and `gWidgetsRGtk2`. If you really want to use RQDA again, we developped a docker image that can be used at this usage : <https://gitlab.com/FrdVnW/dockerqda> (info in the readme file).

<sup><a id="fn.3" href="#fnr.3">3</a></sup> You can use the spreadsheet in <./inst/concepts_builder.xlsx>, screencast for that part here <https://youtu.be/aun75Y-uI6Q>
