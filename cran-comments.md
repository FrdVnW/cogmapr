## Test environments
* local R installation, R 4.1.2
* ubuntu 16.04 (on travis-ci), R 4.1.2
* win-builder (devel)

## R CMD check results

0 errors | 0 warnings | 1 note

* Version 0.9.2 and 0.9.3 : linked to demand from Davis Vaughan
	Hi Frédéric,

	We are updating `tidyr::replace_na()` to utilize the vctrs package, and that results in slightly stricter / more correct type conversions. See https://github.com/tidyverse/tidyr/pull/1219.

	Your package was flagged in our revdeps, so I have put together a pull request that fixes the issue and describes it in more detail. 
	https://github.com/DavisVaughan/cogmapr/pull/1

	We would greatly appreciate if you could merge this into your own codebase and send out a patch release of cogmapr so we can send tidyr to CRAN!

	Thanks,
	Davis Vaughan

